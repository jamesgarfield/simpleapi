package routes

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

func internalServerError(ctx context.Context, w http.ResponseWriter, err error, logmsg string, v ...interface{}) {
	log.Error(ctx, err, logmsg, v...)
	w.WriteHeader(http.StatusInternalServerError)
}

func notFoundError(ctx context.Context, w http.ResponseWriter, err error, logmsg string, v ...interface{}) {
	log.Error(ctx, err, logmsg, v...)
	w.WriteHeader(http.StatusNotFound)
}

func writeJSON(ctx context.Context, w http.ResponseWriter, v interface{}) {
	b := &bytes.Buffer{}
	if err := json.NewEncoder(b).Encode(v); err != nil {
		internalServerError(ctx, w, err, "encoding response")
		return
	}

	if _, err := w.Write(b.Bytes()); err != nil {
		log.Error(ctx, err, "writing response")
		return
	}
}
