package routes

import (
	"errors"
	"net/http"
	"time"
)

var (
	ErrServerError = errors.New("server error")
)

func LoggingMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		start := time.Now()
		si := &statusInterceptor{ResponseWriter: w}
		next.ServeHTTP(si, req)
		ms := time.Now().Sub(start)

		status := si.status
		if status >= http.StatusInternalServerError && status != http.StatusNotImplemented {
			log.Error(ctx, ErrServerError, "%s %s %d %s", req.Method, req.URL.Path, status, ms.String())
		} else {
			log.Info(req.Context(), "%s %s %d %s", req.Method, req.URL.Path, status, ms.String())
		}

	})
}

type statusInterceptor struct {
	status int
	http.ResponseWriter
}

func (si *statusInterceptor) WriteHeader(status int) {
	si.status = status
	si.ResponseWriter.WriteHeader(status)
}
