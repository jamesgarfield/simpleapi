package routes

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/jamesgarfield/simpleapi/logging"
	"bitbucket.org/jamesgarfield/simpleapi/products"

	"github.com/gorilla/mux"
)

var (
	log = logging.NewLogger("server")

	ErrMissingPathVariable = errors.New("missing required path variable")
	ErrInvalidID           = errors.New("invalid path ID")
)

type ProductListDS interface {
	GetProducts(context.Context) ([]products.Product, error)
	CreateProduct(context.Context, products.Product) (products.Product, error)
}

type ProductListHandler struct {
	ProductListDS
}

func (h ProductListHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	switch req.Method {
	case http.MethodGet:
		h.HandleGet(ctx, w, req)
		return
	case http.MethodPost:
		h.HandlePost(ctx, w, req)
		return
	default:
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
}

func (h ProductListHandler) HandleGet(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	products, err := h.GetProducts(ctx)
	if err != nil {
		internalServerError(ctx, w, err, "getting products")
		return
	}

	writeJSON(ctx, w, products)
}

func (h ProductListHandler) HandlePost(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	in := products.Product{}
	if err := json.NewDecoder(req.Body).Decode(&in); err != nil {
		internalServerError(ctx, w, err, "decoding product body")
		return
	}

	result, err := h.CreateProduct(ctx, in)
	if err != nil {
		internalServerError(ctx, w, err, "getting products")
		return
	}

	writeJSON(ctx, w, result)
}

type ProductDS interface {
	GetProduct(ctx context.Context, id int) (products.Product, error)
}

type ProductHandler struct {
	ProductDS
}

func (h ProductHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	switch req.Method {
	case http.MethodGet:
		h.HandleGet(ctx, w, req)
		return
	default:
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
}

func (h ProductHandler) HandleGet(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	productID, ok := vars["product_id"]
	if !ok {
		internalServerError(ctx, w, ErrMissingPathVariable, "product_id")
		return
	}

	id, err := strconv.Atoi(productID)
	if err != nil {
		notFoundError(ctx, w, ErrInvalidID, "product_id: %v", productID)
		return
	}

	product, err := h.GetProduct(ctx, id)
	switch err {
	case nil: //no-op
	case sql.ErrNoRows:
		notFoundError(ctx, w, err, "couldn't find product %d", id)
		return
	default:
		internalServerError(ctx, w, err, "unknown error")
		return
	}

	writeJSON(ctx, w, product)
}
