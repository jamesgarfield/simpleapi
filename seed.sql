BEGIN;

TRUNCATE TABLE products CASCADE;

INSERT INTO products
    (id, brand, name, msrp, description)
VALUES
    (1, 'Memaw''s Kitchen', 'Lemon Cooler Cookies', 4.50, 'Memaw''s original recipe Lemon Coolers are a sweet and zesty treat. Light and crispy lemon cookies that are dusted with the right amount of sweetness. Tiny lemon cookies anytime? What could be cooler?'),
    (2, 'Nooma', 'Organic Sport Energy Drinks', 2.99, 'Our organic pre-workout is loaded with a powerful combination of caffeine from organic green coffee beans, electrolytes from coconut water and Himalayan pink salt, and an adaptogen blend that delivers a boost of energy and focus. It’s an energy drink you can feel and feel good about.'),
    (3, 'Miracle Noodle', 'SugaVida Turmeric Latte', 14.99, 'A modern-day health elixir enriched with ancient healing properties, SugaVida™ Turmeric Lattes are naturally sweet, delicately spiced, and bursting with B vitamins, essential minerals, and health-boosting ingredients to promote an overall feeling of well-being.')
;

SELECT setval('products_id_seq', 3, true);

INSERT INTO variants
    (id, product_id, name)
VALUES
    (1, 2, 'Flavor'),
    (2, 3, 'Flavor'),
    (3, 3, 'Quantity')
;

SELECT setval('variants_id_seq',3, true);

INSERT INTO variant_values
    (id, variant_id, value)
VALUES
    (1, 1, 'Dragon Fruit'),
    (2, 1, 'Pineapple Mango'),
    (3, 1, 'Lemon Lime'),
    (4, 1, 'Tangerine'),
    (5, 2, 'Original'),
    (6, 2, 'Cardamom'),
    (7, 2, 'Spicy Ginger'),
    (8, 3, '6 Count'),
    (9, 3, '18 Count'),
    (10, 3, '30 Count')
;

SELECT setval('variant_values_id_seq', 10, true);

COMMIT;