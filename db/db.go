package db

import (
	"context"

	"bitbucket.org/jamesgarfield/simpleapi/logging"
	"bitbucket.org/jamesgarfield/simpleapi/products"
	"github.com/jmoiron/sqlx"
)

var (
	log = logging.NewLogger("db")
)

type Datastore struct {
	*sqlx.DB
}

func NewDatastore(conn string) (*Datastore, error) {
	db, err := sqlx.Connect("pgx", conn)
	if err != nil {
		return nil, err
	}
	return &Datastore{db}, nil
}

const productTmpl = `
WITH product_variants AS (
	SELECT v.product_id, JSONB_AGG(v) AS variants
	FROM (
		SELECT
			variants.product_id,
			variants.id,
			variants.name,
			JSONB_AGG(variant_values) AS values
		FROM variants
		INNER JOIN variant_values ON variant_values.variant_id = variants.id
		WHERE true
		{{if .ProductID}}
			AND variants.product_id = {{Arg .ProductID}}
		{{end}}
		GROUP BY variants.id, variants.product_id, variants.name
	) v
	GROUP BY v.product_id
)
SELECT products.*, product_variants.variants
FROM products
LEFT OUTER JOIN product_variants ON product_variants.product_id = products.id
WHERE true
{{if .ProductID}}
	AND products.id = {{Arg .ProductID}}
{{end}}
`

type productParams struct {
	ProductID *int
}

func (ds Datastore) GetProducts(ctx context.Context) ([]products.Product, error) {
	sql, args, err := NewTemplate("GetProducts", productTmpl).Execute(productParams{})
	if err != nil {
		return nil, err
	}
	rows, err := ds.QueryxContext(ctx, sql, args...)
	if err != nil {
		return nil, err
	}

	results := []products.Product{}
	for rows.Next() {
		p := products.Product{}
		if err := rows.StructScan(&p); err != nil {
			return nil, err
		}
		results = append(results, p)
	}
	return results, nil
}

func (ds Datastore) GetProduct(ctx context.Context, id int) (products.Product, error) {
	sql, args, err := NewTemplate("GetProduct", productTmpl).Execute(productParams{&id})
	if err != nil {
		return products.Product{}, err
	}
	p := products.Product{}
	err = ds.QueryRowxContext(ctx, sql, args...).StructScan(&p)
	if err != nil {
		return products.Product{}, err
	}

	return p, nil
}

func (ds Datastore) CreateProduct(ctx context.Context, p products.Product) (products.Product, error) {
	var productID int

	tx, err := ds.BeginTxx(ctx, nil)
	if err != nil {
		return products.Product{}, err
	}
	transact := func(tx *sqlx.Tx) error {
		err := tx.QueryRowxContext(ctx, `
			INSERT INTO products
				(name, brand, description, msrp)
			VALUES
				($1, $2, $3, $4)
			RETURNING
				(id)
		`,
			p.Name, p.Brand, p.Description, p.MSRP,
		).Scan(&productID)
		if err != nil {
			return err
		}

		for _, variant := range p.Variants {
			var variantID int
			err := tx.QueryRowxContext(ctx, `
				INSERT INTO variants
					(product_id, name)
				VALUES
					($1, $2)
				RETURNING
					(id)
			`,
				productID, variant.Name,
			).Scan(&variantID)
			if err != nil {
				return err
			}

			for _, val := range variant.Values {
				_, err := tx.ExecContext(ctx, `
					INSERT INTO variant_values
						(variant_id, value)
					VALUES
						($1, $2)
				`,
					variantID, val.Value,
				)
				if err != nil {
					return err
				}
			}
		}
		return nil
	}

	if err := transact(tx); err != nil {
		if txerr := tx.Rollback(); txerr != nil {
			log.Error(ctx, txerr, "while rolling back transaction")
		}
		return products.Product{}, err
	}
	if err := tx.Commit(); err != nil {
		return products.Product{}, err
	}

	return ds.GetProduct(ctx, productID)
}
