package db

import (
	"bytes"
	"fmt"
	"html/template"
	"reflect"
)

func NewTemplate(name string, templates ...string) *Template {
	args := &ArgBuilder{}
	fm := template.FuncMap{
		"Arg": args.Arg,
	}
	return &Template{
		Name:       name,
		Templates:  templates,
		Funcs:      fm,
		ArgBuilder: args,
	}
}

type Template struct {
	Name       string
	Templates  []string
	Funcs      template.FuncMap
	ArgBuilder *ArgBuilder
}

func (t *Template) Execute(params interface{}) (string, []interface{}, error) {
	tmp := template.New(t.Name).Funcs(t.Funcs)
	var err error
	for _, x := range t.Templates {
		tmp, err = tmp.Parse(x)
		if err != nil {
			return "", nil, err
		}
	}

	var sql bytes.Buffer
	if err := tmp.Execute(&sql, params); err != nil {
		return "", nil, err
	}
	return sql.String(), t.ArgBuilder.Args, nil
}

type ArgBuilder struct {
	Args []interface{}
}

func (ab *ArgBuilder) Arg(val interface{}) string {
	valueOf := reflect.ValueOf(val)
	if valueOf.Kind() == reflect.Ptr {
		ab.Args = append(ab.Args, valueOf.Elem().Interface())
	} else {
		ab.Args = append(ab.Args, val)
	}

	bindVar := fmt.Sprintf("$%d", len(ab.Args))
	return bindVar
}
