BEGIN;

CREATE TABLE products (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    brand VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    msrp NUMERIC(12,2) NOT NULL,

    CONSTRAINT valid_pricing CHECK (msrp > 0),
    UNIQUE(brand, name)
);

CREATE INDEX product_brand_name ON products USING btree (brand, name);
CREATE INDEX product_name ON products USING btree (name);

CREATE TABLE variants (
    id SERIAL PRIMARY KEY,
    product_id INT NOT NULL REFERENCES products(id),
    name VARCHAR NOT NULL
);

CREATE INDEX variant_product ON variants USING btree (product_id);

CREATE TABLE variant_values (
    id SERIAL PRIMARY KEY,
    variant_id INT NOT NULL REFERENCES variants(id),
    value VARCHAR NOT NULL
);

COMMIT;