package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/jamesgarfield/simpleapi/db"
	"bitbucket.org/jamesgarfield/simpleapi/logging"
	"bitbucket.org/jamesgarfield/simpleapi/routes"
	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
)

var (
	log = logging.NewLogger("simpleapi")
)

func main() {
	ctx := context.Background()
	err := serve(ctx, "localhost:5432")
	if err != nil {
		log.Error(ctx, err, "from serving")
		os.Exit(1)
	}
}

func serve(ctx context.Context, dbConn string) error {
	ds, err := db.NewDatastore(dbConn)
	if err != nil {
		return err
	}
	defer ds.Close()

	r := mux.NewRouter()

	r.Use(routes.LoggingMW)

	r.Handle("/products", routes.ProductListHandler{ds})
	r.Handle("/products/{product_id:[0-9]+}", routes.ProductHandler{ds})

	server := &http.Server{
		Addr:    "0.0.0.0:9876",
		Handler: r,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Error(ctx, err, "while serving")
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until interupt
	<-c

	var wait time.Duration = time.Second * 10
	ctx, cancel := context.WithTimeout(ctx, wait)
	defer cancel()
	server.Shutdown(ctx)
	log.Info(ctx, "shutting down")

	return nil
}
