package logging

import (
	"context"
	"fmt"
	"log"
	"os"
)

type Logger interface {
	Error(ctx context.Context, err error, format string, v ...interface{})
	Info(ctx context.Context, format string, v ...interface{})
}

func NewLogger(prefix string) Logger {
	const flags = log.Ldate | log.Ltime | log.Lmsgprefix
	return &logger{log.New(os.Stderr, fmt.Sprintf("%s ", prefix), flags)}
}

type logger struct {
	*log.Logger
}

func (l *logger) Error(ctx context.Context, err error, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	l.Logger.Printf("ERROR: %s %v", msg, err)
}

func (l *logger) Info(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	l.Logger.Printf("INFO: %s", msg)
}
