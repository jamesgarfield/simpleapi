package products

import (
	"database/sql/driver"
	"encoding/json"
	"math/big"

	"github.com/pkg/errors"
)

type Product struct {
	ID          int
	Name        string
	Brand       string
	Description string
	MSRP        Money
	Variants    Variants
}

type Variant struct {
	ID     int
	Name   string
	Values VarVals
}

type Variants []Variant

func (v *Variants) Scan(i interface{}) error {
	switch val := i.(type) {
	case nil:
		return nil
	case []byte:
		if len(val) == 0 || val == nil {
			return nil
		}
		return json.Unmarshal(val, v)
	case string:
		return v.Scan([]byte(val))
	default:
		return errors.Errorf("invalid type for Variants %T", i)
	}
}

type VarVal struct {
	ID    int
	Value string
}

type VarVals []VarVal

func (v *VarVals) Scan(i interface{}) error {
	switch val := i.(type) {
	case []byte:
		return json.Unmarshal(val, v)
	case string:
		return v.Scan([]byte(val))
	default:
		return errors.Errorf("invalid type for VarVals %T", i)
	}
}

type Money struct {
	big.Rat
}

func (m *Money) UnmarshalJSON(data []byte) error {
	var n json.Number
	err := json.Unmarshal(data, &n)
	if err != nil {
		return err
	}
	r, ok := new(big.Rat).SetString(n.String())
	if !ok {
		return errors.New("invalid numeric")
	}
	m.Rat = *r
	return nil
}

func (m Money) MarshalJSON() ([]byte, error) {
	n := json.Number(m.FloatString(2))
	return json.Marshal(n)
}

func (m *Money) Scan(i interface{}) error {
	switch n := i.(type) {
	case int64:
		m.SetInt64(n)
	case float64:
		m.SetFloat64(n)
	case []byte:
		return m.Scan(string(n))
	case string:
		_, ok := m.SetString(n)
		if !ok {
			return errors.Errorf("invalid numeric string: %s", n)
		}
	default:
		return errors.Errorf("invalid numeric type %T", i)
	}
	return nil
}

func (m Money) Value() (driver.Value, error) {
	f, _ := m.Float64()
	return f, nil
}

func (m Money) String() string {
	return m.FloatString(2)
}
